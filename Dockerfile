FROM node:18.12.1
# Creating the directory named “testunbox” by running the below command
RUN mkdir testunbox
# Below command copies all the files from the repository into the “testunbox” directory
COPY . /testunbox
# Below command is used to update the packages and libraries in Ubuntu or Linux OS
RUN apt-get update
# Since I have used selenium standalone services to run my test scripts I would require JDK to be installed in the system, so to install JDK in the Linux or Ubuntu platform I would require to run the below command.
RUN apt-get install -y openjdk-11-jdk
# Below command is used to specify the URL to download the chrome browser and also specifying the key to run the chrome browser and also this below command had to be used because the CI pipeline in the gitlab is failing to locate the chrome binary which is already present in the node modules.
RUN curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add
# Below command is listing out all the stable versions of chrome and getting store in the specified location
RUN bash -c "echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' >> /etc/apt/sources.list.d/google-chrome.list"
RUN apt-get update
# Below command is used to install the latest stable version of chrome from the above mentioned URL for the test scripts execution.
RUN apt install -y google-chrome-stable
# Below command is used to set the key for Firefox browser, as mocha framework supports cross browser testing.
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys A6DCF7707EBC211F
# Below command is used to install Ubuntu commands like apt-add-repository
RUN apt install -y software-properties-common
# Below command is adding the official Firefox ppa repository to download the Firefox browser for Ubuntu.
RUN apt-add-repository "deb http://ppa.launchpad.net/ubuntu-mozilla-security/ppa/ubuntu focal main"
# Below command is updating the added libraries
RUN apt update
# Below command is installing the latest stable version of Firefox
RUN apt install -y firefox
# Below command is provides the selenium-server-standalone services and libraries
RUN wget https://selenium-release.storage.googleapis.com/3.141/selenium-server-standalone-3.141.59.jar
# Below command is used to move into testunbox directory where the complete files of gitlab repository is copied into and running npm install command to install the dependencies that are mentioned in the package.json file.
RUN cd /testunbox/ \
&&  npm install
# Below command is used to provide appropriate permission to execute the commands
RUN chmod 655 /testunbox/node_modules/.bin/*
# Making testunbox as working directory in order to execute test scripts from here
WORKDIR /testunbox
# Below command is used to run all the test scripts mentioned in the specs property in wdio.conf.js file
ENTRYPOINT ['npm', 'run', 'test']
