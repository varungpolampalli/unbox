//IMPORTS
const fs = require("fs")
let Testdata = JSON.parse(fs.readFileSync('./test/specs/Testdata/data.json'))
const workflowLibrary = require('../../workflowLibrary/workflowLibrary')
//SCRIPT
describe(`Verify user should be able to add/edit/delete dashboard users from myteam screen`, async () => {
    Testdata.forEach(({ TESTUSN, TESTPWD, TESTPWDMOD }) => {
        it(`Signin and get the title,Click on add button, enter username ,password and click on create,
    Click on edit button,modify password and click on update,Click delete button to whom you want to delete`, async () => {
            await workflowLibrary.loginOperation(process.env.ROOTUSN, process.env.ROOTPWD)
            await workflowLibrary.successMessageValidation("Login Successful")
            await workflowLibrary.createUser(TESTUSN, TESTPWD)
            await workflowLibrary.successMessageValidation("User Created Successfully")
            await workflowLibrary.modifyUser(TESTUSN, TESTPWDMOD)
            await workflowLibrary.successMessageValidation("User Updated Successfully")
            await workflowLibrary.deleteUser(TESTUSN)
            await workflowLibrary.successMessageValidation("Success - User successfully deleted")
        })
    })
})