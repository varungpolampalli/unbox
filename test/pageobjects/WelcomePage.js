require("dotenv").config()
class WelcomePage {
    get userIDTextField() {
        return $("//input[@type='email']")
    }

    get userPasswordTextField() {
        return $("//input[@type='password']")
    }

    get loginButton() {
        return $("//button[.='LOGIN']")
    }

    get success() {
       return $("//div[@class='Toastify__toast Toastify__toast--success']/descendant::div[@role='alert']")
    }

    get closeMessage() {
        return $("//*[name()='svg']")
    }

}
module.exports = new WelcomePage()
