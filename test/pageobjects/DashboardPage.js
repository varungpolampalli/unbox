const logger = require("../logger");

class DashboardPage {

    get myTeamLink() {
        return browser.$("//div[@id='team']")
    }

    get systemControl() {
        browser.waitUntil(() => browser.$("//div[@id='system']").isDisplayed(), {
            timeout: 3000,
            timeoutMsg: "Not loaded"
        })
        return $("//div[@id='system']")
    }
//system control
    get reports() {
        browser.waitUntil(() => browser.$("//div[@id='reports']"), {
            timeout: 3000,
            timeoutMsg: "Not loaded"
        })
        return browser.$("//div[@id='reports']")
    }

    get pageHeader(){
        return $("//div[@class=' header ']/descendant::div[@class='header-heading']")
    }

    async systemControlOption(option) {
        return $(`//span[@class='side-content' and .='${option}']`)
    }

    async reportsOption(option) {
        return browser.$(`//span[@class='side-content' and .='${option}']/ancestor::a`)
    }

}
module.exports = new DashboardPage()